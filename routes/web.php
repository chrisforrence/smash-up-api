<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->options('/', function () {
    return [
        'GET' => [
            'description' => 'Randomly assigns factions to a given number of players',
            'parameters' => [
                'players' => [
                    'type' => 'integer',
                    'required' => true,
                    'description' => 'How many players will we give random factions to?'
                ],
                'sets' => [
                    'type' => 'string[]',
                    'required' => false,
                    'description' => 'An array of set slugs; if this parameter is not provided, will draw from all released sets'
                ],
            ],
            'example' => [
                'request' => [
                    'players' => 2
                ],
                'response' => [
                    'data' => [
                        'requires_madness' => false,
                        'requires_treasure' => false,
                        'requires_monsters' => false,
                        'players' => [
                            ['Dinosaurs', 'Grannies'],
                            ['Geeks', 'Orcs']
                        ]
                    ]
                ]
            ]
        ]
    ];
});
$router->get('/', function (Request $request) {
    $response = [
        'data' => [
            'requires_madness' => false,
            'requires_treasure' => false,
            'requires_monsters' => false,
            'players' => [],
        ]
    ];
    $this->validate($request, [
        'players' => 'required|integer|min:2|max:4',
    ]);
    $sets = app('db')->table('sets')
        ->when($request->input('sets'), function ($query) use ($request) {
            return $query->whereIn('slug', $request->input('sets'));
        })
        ->pluck('id');
    $factions = app('db')->table('factions')
        ->whereIn('set_id', $sets)
        ->get()->toArray();

    if (count($factions) < intval($request->input('players')) * 2) {
        return new Illuminate\Http\JsonResponse(['players' => 'Too few players'], 422);
    }
    shuffle($factions);

    $players = array_fill(0, $request->input('players'), []);
    for ($i = 0; $i < count($players); $i++) {
        $faction = array_pop($factions);
        $response['data']['requires_madness'] = $response['data']['requires_madness'] || $faction->requires_madness;
        $response['data']['requires_treasure'] = $response['data']['requires_treasure'] || $faction->requires_treasure;
        $response['data']['requires_monsters'] = $response['data']['requires_monsters'] || $faction->requires_monsters;
        $players[$i][] = $faction->name;
    }
    for ($i = count($players) - 1; $i >= 0; $i--) {
        $faction = array_pop($factions);
        $response['data']['requires_madness'] = $response['data']['requires_madness'] || $faction->requires_madness;
        $response['data']['requires_treasure'] = $response['data']['requires_treasure'] || $faction->requires_treasure;
        $response['data']['requires_monsters'] = $response['data']['requires_monsters'] || $faction->requires_monsters;
        $players[$i][] = $faction->name;
    }
    $response['data']['players'] = $players;
    return $response;
});

$router->options('/sets', function () {
    return [
        'GET' => [
            'description' => 'Gets the list of available Smash Up! sets',
            'parameters' => [],
            'example' => [
                'request' => [],
                'response' => [
                    'data' => ['base-set', 'slug-1']
                ]
            ]
        ]
    ];
});
$router->get('/sets', function () {
    return [
        'data' => app('cache')->remember('sets', 60 * 12, function() {
            return app('db')->table('sets')
                ->join('factions', 'factions.set_id', '=', 'sets.id')
                ->whereNull('released_on')
                ->orWhere('released_on', '<=', Carbon\Carbon::today())
                ->select(app('db')->raw('sets.name, sets.slug, COUNT(factions.id) AS factions'))
                ->groupBy('sets.name', 'sets.slug')
                ->get();
        })
    ];
});
