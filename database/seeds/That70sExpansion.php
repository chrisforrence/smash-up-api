<?php

use Illuminate\Database\Seeder;

class That70sExpansion extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $set_id = app('db')->table('sets')->insertGetId([
            'name' => "That '70s Expansion",
            'slug' => str_slug("That '70s Expansion"),
            'released_on' => '2018-03-21',
        ]);

        app('db')->table('factions')->insert([
            ['set_id' => $set_id, 'name' => 'Disco Dancers'],
            ['set_id' => $set_id, 'name' => 'Kung Fu Fighters'],
            ['set_id' => $set_id, 'name' => 'Truckers'],
            ['set_id' => $set_id, 'name' => 'Vigilantes'],
        ]);
    }
}
