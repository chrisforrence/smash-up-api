<?php

use Illuminate\Database\Seeder;

class DropSetsAndFactions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app('db')->table('factions')->truncate();
        app('db')->table('sets')->truncate();
    }
}
