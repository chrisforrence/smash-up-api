<?php

use Illuminate\Database\Seeder;

class CoreGame extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $set_id = app('db')->table('sets')->insertGetId([
            'name' => 'Core Game',
            'slug' => str_slug('Core Game'),
        ]);

        app('db')->table('factions')->insert([
            ['set_id' => $set_id, 'name' => 'Aliens'],
            ['set_id' => $set_id, 'name' => 'Dinosaurs'],
            ['set_id' => $set_id, 'name' => 'Ninjas'],
            ['set_id' => $set_id, 'name' => 'Pirates'],
            ['set_id' => $set_id, 'name' => 'Robots'],
            ['set_id' => $set_id, 'name' => 'Tricksters'],
            ['set_id' => $set_id, 'name' => 'Wizards'],
            ['set_id' => $set_id, 'name' => 'Zombies'],
        ]);
    }
}
