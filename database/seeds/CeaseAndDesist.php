<?php

use Illuminate\Database\Seeder;

class CeaseAndDesist extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $set_id = app('db')->table('sets')->insertGetId([
            'name' => 'Cease and Desist',
            'slug' => str_slug('Cease and Desist'),
        ]);

        app('db')->table('factions')->insert([
            ['set_id' => $set_id, 'name' => 'Astroknights'],
            ['set_id' => $set_id, 'name' => 'Changerbots'],
            ['set_id' => $set_id, 'name' => 'Ignobles'],
            ['set_id' => $set_id, 'name' => 'Star Roamers'],
        ]);
    }
}
