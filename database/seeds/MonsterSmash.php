<?php

use Illuminate\Database\Seeder;

class MonsterSmash extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $set_id = app('db')->table('sets')->insertGetId([
            'name' => 'Monster Smash',
            'slug' => str_slug('Monster Smash'),
        ]);

        app('db')->table('factions')->insert([
            ['set_id' => $set_id, 'name' => 'Giant Ants'],
            ['set_id' => $set_id, 'name' => 'Mad Scientists'],
            ['set_id' => $set_id, 'name' => 'Vampires'],
            ['set_id' => $set_id, 'name' => 'Werewolves'],
        ]);
    }
}
