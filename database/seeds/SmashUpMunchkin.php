<?php

use Illuminate\Database\Seeder;

class SmashUpMunchkin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $set_id = app('db')->table('sets')->insertGetId([
            'name' => 'Smash Up: Munchkin',
            'slug' => str_slug('Smash Up: Munchkin'),
        ]);

        app('db')->table('factions')->insert([
            ['set_id' => $set_id, 'requires_treasure' => true, 'requires_monsters' => true, 'name' => 'Clerics'],
            ['set_id' => $set_id, 'requires_treasure' => true, 'requires_monsters' => true, 'name' => 'Dwarves'],
            ['set_id' => $set_id, 'requires_treasure' => true, 'requires_monsters' => true, 'name' => 'Elves'],
            ['set_id' => $set_id, 'requires_treasure' => true, 'requires_monsters' => true, 'name' => 'Halflings'],
            ['set_id' => $set_id, 'requires_treasure' => true, 'requires_monsters' => true, 'name' => 'Mages'],
            ['set_id' => $set_id, 'requires_treasure' => true, 'requires_monsters' => true, 'name' => 'Orcs'],
            ['set_id' => $set_id, 'requires_treasure' => true, 'requires_monsters' => true, 'name' => 'Thieves'],
            ['set_id' => $set_id, 'requires_treasure' => true, 'requires_monsters' => true, 'name' => 'Warriors'],
        ]);
    }
}
