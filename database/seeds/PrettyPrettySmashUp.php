<?php

use Illuminate\Database\Seeder;

class PrettyPrettySmashUp extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $set_id = app('db')->table('sets')->insertGetId([
            'name' => 'Pretty Pretty Smash Up',
            'slug' => str_slug('Pretty Pretty Smash Up'),
        ]);

        app('db')->table('factions')->insert([
            ['set_id' => $set_id, 'name' => 'Fairies'],
            ['set_id' => $set_id, 'name' => 'Kitty Cats'],
            ['set_id' => $set_id, 'name' => 'Mythic Horses'],
            ['set_id' => $set_id, 'name' => 'Princesses'],
        ]);
    }
}
