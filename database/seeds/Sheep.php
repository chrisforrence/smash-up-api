<?php

use Illuminate\Database\Seeder;

class Sheep extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $set_id = app('db')->table('sets')->insertGetId([
            'name' => 'Sheep!',
            'slug' => str_slug('Sheep!'),
        ]);

        app('db')->table('factions')->insert([
            ['set_id' => $set_id, 'name' => 'Sheep'],
        ]);
    }
}
