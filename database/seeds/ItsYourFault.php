<?php

use Illuminate\Database\Seeder;

class ItsYourFault extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $set_id = app('db')->table('sets')->insertGetId([
            'name' => "It's Your Fault!",
            'slug' => str_slug("It's Your Fault!"),
        ]);

        app('db')->table('factions')->insert([
            ['set_id' => $set_id, 'name' => 'Dragons'],
            ['set_id' => $set_id, 'name' => 'Mythic Greeks'],
            ['set_id' => $set_id, 'name' => 'Sharks'],
            ['set_id' => $set_id, 'name' => 'Superheroes'],
            ['set_id' => $set_id, 'name' => 'Tornados'],
        ]);
    }
}
