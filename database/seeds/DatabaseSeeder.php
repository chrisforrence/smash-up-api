<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Seeding the database...');
        $this->call('DropSetsAndFactions');
        $this->call('CoreGame');
        $this->call('AwesomeLevel9000');
        $this->call('TheObligatoryCthulhuSet');
        $this->call('TheBigGeekyBox');
        $this->call('MonsterSmash');
        $this->call('PrettyPrettySmashUp');
        $this->call('SmashUpMunchkin');
        $this->call('ItsYourFault');
        $this->call('CeaseAndDesist');
        $this->call('WhatWereWeThinking');
        $this->call('AllStarsEventKit');
        $this->call('BigInJapan');
        $this->call('Sheep');
        $this->call('That70sExpansion');
        $this->call('OopsYouDidItAgain');
        $this->command->info('All done!');
    }
}
