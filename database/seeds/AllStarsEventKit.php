<?php

use Illuminate\Database\Seeder;

class AllStarsEventKit extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $set_id = app('db')->table('sets')->insertGetId([
            'name' => 'All Stars Event Kit',
            'slug' => str_slug('All Stars Event Kit'),
        ]);

        app('db')->table('factions')->insert([
            ['set_id' => $set_id, 'name' => 'Smash Up All Stars'],
        ]);
    }
}
