<?php

use Illuminate\Database\Seeder;

class ScienceFictionDoubleFeature extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $set_id = app('db')->table('sets')->insertGetId([
            'name' => 'Science Fiction Double Feature',
            'slug' => str_slug('Science Fiction Double Feature'),
        ]);

        app('db')->table('factions')->insert([
            ['set_id' => $set_id, 'name' => 'Cyborg Apes'],
            ['set_id' => $set_id, 'name' => 'Shapeshifters'],
            ['set_id' => $set_id, 'name' => 'Super Spies'],
            ['set_id' => $set_id, 'name' => 'Time Travelers'],
        ]);
    }
}
