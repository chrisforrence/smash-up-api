<?php

use Illuminate\Database\Seeder;

class AwesomeLevel9000 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $set_id = app('db')->table('sets')->insertGetId([
            'name' => 'Awesome Level 9000',
            'slug' => str_slug('Awesome Level 9000'),
        ]);

        app('db')->table('factions')->insert([
            ['set_id' => $set_id, 'name' => 'Bear Cavalry'],
            ['set_id' => $set_id, 'name' => 'Ghosts'],
            ['set_id' => $set_id, 'name' => 'Killer Plants'],
            ['set_id' => $set_id, 'name' => 'Steampunks'],
        ]);
    }
}
