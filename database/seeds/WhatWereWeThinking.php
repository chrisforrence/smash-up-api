<?php

use Illuminate\Database\Seeder;

class WhatWereWeThinking extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $set_id = app('db')->table('sets')->insertGetId([
            'name' => 'What Were We Thinking?',
            'slug' => str_slug('What Were We Thinking?'),
        ]);

        app('db')->table('factions')->insert([
            ['set_id' => $set_id, 'name' => 'Explorers'],
            ['set_id' => $set_id, 'name' => 'Grannies'],
            ['set_id' => $set_id, 'name' => 'Rock Stars'],
            ['set_id' => $set_id, 'name' => 'Teddy Bears'],
        ]);
    }
}
