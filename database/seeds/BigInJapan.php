<?php

use Illuminate\Database\Seeder;

class BigInJapan extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $set_id = app('db')->table('sets')->insertGetId([
            'name' => 'Big in Japan',
            'slug' => str_slug('Big in Japan'),
        ]);

        app('db')->table('factions')->insert([
            ['set_id' => $set_id, 'name' => 'Itty Critters'],
            ['set_id' => $set_id, 'name' => 'Kaiju'],
            ['set_id' => $set_id, 'name' => 'Magical Girls'],
            ['set_id' => $set_id, 'name' => 'Mega Troopers'],
        ]);
    }
}
