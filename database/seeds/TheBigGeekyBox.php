<?php

use Illuminate\Database\Seeder;

class TheBigGeekyBox extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $set_id = app('db')->table('sets')->insertGetId([
            'name' => 'The Big Geeky Box',
            'slug' => str_slug('The Big Geeky Box'),
        ]);

        app('db')->table('factions')->insert([
            ['set_id' => $set_id, 'name' => 'Geeks'],
        ]);
    }
}
