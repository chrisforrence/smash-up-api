<?php

use Illuminate\Database\Seeder;

class TheObligatoryCthulhuSet extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $set_id = app('db')->table('sets')->insertGetId([
            'name' => 'The Obligatory Cthulhu Set',
            'slug' => str_slug('The Obligatory Cthulhu Set'),
        ]);

        app('db')->table('factions')->insert([
            ['set_id' => $set_id, 'requires_madness' => true, 'name' => 'Elder Things'],
            ['set_id' => $set_id, 'requires_madness' => true, 'name' => 'Innsmouth'],
            ['set_id' => $set_id, 'requires_madness' => true, 'name' => 'Minions of Cthulhu'],
            ['set_id' => $set_id, 'requires_madness' => true, 'name' => 'Miskatonic University'],
        ]);
    }
}
