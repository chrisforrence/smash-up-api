<?php

use Illuminate\Database\Seeder;

class OopsYouDidItAgain extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $set_id = app('db')->table('sets')->insertGetId([
            'name' => 'Oops You Did It Again',
            'slug' => str_slug('Oops You Did It Again'),
            'released_on' => '2018-09-01'
        ]);

        app('db')->table('factions')->insert([
            ['set_id' => $set_id, 'name' => 'Vikings'],
            ['set_id' => $set_id, 'name' => 'Cowboys'],
            ['set_id' => $set_id, 'name' => 'Ancient Egyptians'],
            ['set_id' => $set_id, 'name' => 'Samurai'],
        ]);
    }
}
